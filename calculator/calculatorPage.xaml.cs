﻿using System;
using System.Diagnostics;
using Xamarin.Forms;

namespace calculator
{
    public partial class calculatorPage : ContentPage
    {

        public int total = 0;
        public string input = "";
        public string ope = "";

        public calculatorPage()
        {
            InitializeComponent();
        }

        void AddNumber(object sender, EventArgs e)
        {
            var button = sender as Button;
            input += button.Text;
            value.Text = input;
        }

        void Calcule(string button)
        {
            if (string.IsNullOrEmpty(input)) {
                return;
            }
            if (total == 0)
            {
                total = int.Parse(input);
            }
            else if (button.Equals("+"))
            {
                total += int.Parse(input);
            }
            else if (button.Equals("-"))
            {
                total -= int.Parse(input);
            }
            else if (button.Equals("/"))
            {
                if (input.Equals("0"))
                    return;
                total /= int.Parse(input);
            }
            else if (button.Equals("X"))
            {
                total *= int.Parse(input);
            }
        }

        void Operator(object sender, EventArgs e)
        {
            String button = (sender as Button).Text;

            if (button.Equals("C")) {
                total = 0;
            } else if (button.Equals("=")) {
                Calcule(ope);
                ope = "";
            } else {
                ope = button;
                Calcule(button);
            }
            input = "";
            value.Text = total.ToString();
        }
    }
}
